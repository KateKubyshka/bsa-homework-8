import { callApi } from '../helpers/apiHelper';
import { IFighter } from '../interfaces/IFighter';
import { IFighterDetails } from '../interfaces/IFighterDetails';

export async function getFighters() {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET') as IFighter[];
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id : string) {
  try{
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint,'GET') as IFighterDetails;
    return apiResult;
  }
  catch(error){
    throw error;
  }

}

