import { getFighters } from '../typestcript/services/fightersService'
import { createFighters } from '../typestcript//fightersView';

const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');

export async function startApp() {
  try {
    if(loadingElement){
      loadingElement.style.visibility = 'visible';
    }
    else{
      throw new Error("Loading element is null");
    }
    const fighters = await getFighters();
    const fightersElement = createFighters(fighters);
    if(rootElement){
      rootElement.appendChild(fightersElement);
    }
    else{
      throw new Error("RootElement is null");
    }
  } catch (error) {
    console.warn(error);
    if(rootElement){
      rootElement.innerText = 'Failed to load data';
    }
  } finally {
    if(loadingElement){
      loadingElement.style.visibility = 'hidden';
    }

  }
}
