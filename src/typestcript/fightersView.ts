import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { IFighter } from './interfaces/IFighter';
import { IFighterDetails } from './interfaces/IFighterDetails';
import { getFighterDetails } from './services/fightersService';

export function createFighters(fighters : IFighter[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map<string,IFighterDetails>();

async function showFighterDetails(event : Event, fighter : IFighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId : string) : Promise<IFighterDetails> {
    if(!fightersDetailsCache.has(fighterId)){
      fightersDetailsCache.set(fighterId,await getFighterDetails(fighterId));
    }
     return fightersDetailsCache.get(fighterId)!;
  // get fighter form fightersDetailsCache or use getFighterDetails function
}

function createFightersSelector() {
  const selectedFighters = new Map<string,IFighterDetails>();

  return async function selectFighterForBattle(event : Event, fighter : IFighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((<HTMLInputElement>event.target).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const fighters: IFighterDetails[] = [];
      selectedFighters.forEach((value) => fighters.push(value));
      const winner = fight(fighters[0], fighters[1]);
      showWinnerModal(winner);
    }
  }
}
