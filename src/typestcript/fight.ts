import { IFighterDetails } from "./interfaces/IFighterDetails";

export function fight(firstFighter : IFighterDetails, secondFighter : IFighterDetails) : IFighterDetails {
  let first : IFighterDetails = firstFighter;
  let second : IFighterDetails = secondFighter;
  if(coinToss() === 1){
    first = secondFighter;
    second = firstFighter;
  }
  while(true){
    second.health -= getDamage(first, second);
    if (second.health < 0) {
      second.health = 0;
      return first;
    }
    first.health -= getDamage(second, first);
    if(first.health < 0){
      first.health = 0;
      return second;
    }
  }
}

export function getDamage(attacker : IFighterDetails, enemy : IFighterDetails) {
  const damage = getHitPower(attacker) - getBlockPower(enemy) ;
  return damage > 0 ? damage : 0; 
} 

export function getHitPower(fighter : IFighterDetails) : number {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter : IFighterDetails) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}

export function coinToss() {
  return Math.floor(Math.random() * 2);
}