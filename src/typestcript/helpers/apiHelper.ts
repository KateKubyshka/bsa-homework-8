import { IFighterDetails } from '../interfaces/IFighterDetails';
import { fightersDetails, fighters } from './mockData';
import { IFighter } from '../interfaces/IFighter';

const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI = true;



async function callApi(endpoint : string, method : string) : Promise<IFighterDetails | IFighter[]> {
  const url = API_URL + endpoint;
  const options  : RequestInit = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response : Response) => (response.ok ? <Promise<IJsonResponse>>response.json() : Promise.reject(Error('Failed to load'))))
        .then((result: IJsonResponse) => <IFighterDetails | IFighter[]>JSON.parse(result.content))
        .catch((error : Error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint : string) : Promise<IFighterDetails | IFighter[]>{
  const response = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint : string) {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);

  return fightersDetails.find((it) => it._id === id);
}

export { callApi };
