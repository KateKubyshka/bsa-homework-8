import { ICreateElement } from "../interfaces/ICreateElement";
export function createElement({ tagName, className, attributes = {}} : ICreateElement) : HTMLElement {
  const element = document.createElement(tagName);
  
  if (className) {
    element.classList.add(className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
}