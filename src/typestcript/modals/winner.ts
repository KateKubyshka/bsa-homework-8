import { createElement } from "../helpers/domHelper";
import { IFighterDetails } from "../interfaces/IFighterDetails";
import { showModal } from "./modal";

export  function showWinnerModal(fighter : IFighterDetails) : void {

  const title = `${fighter.name} won!`;
  const bodyElement = createWinner(fighter);
  showModal({ title, bodyElement });
}

function createWinner(fighter: IFighterDetails) {
  const { name } = fighter;
  const fighterDetails = createElement({ tagName: 'div', className: 'winner-model' });
  const winnerHealth = createElement({tagName:'span',className:'winner-health'});
  winnerHealth.innerText = 'Health : ' + fighter.health;
  const image = createElement({tagName:'img',className:'fighter-image',attributes:{src:fighter.source,alt:'winner'}}); 
 
  fighterDetails.append(image, winnerHealth);
  console.log(fighter.name);
  return fighterDetails;
}