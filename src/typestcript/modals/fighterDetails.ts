import { createElement } from '../helpers/domHelper';
import { showModal } from '../modals/modal';
import { IFighterDetails } from '../interfaces/IFighterDetails';

 
export  function showFighterDetailsModal( fighter:IFighterDetails ) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter:IFighterDetails) : HTMLElement {
  const { name } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

  const fighterAttack = createElement({tagName: 'span', className:'fighter-attack'});
  fighterAttack.innerText = 'Attack : '+ fighter.attack;
  const fighterDef = createElement({tagName:'span', className:'fighter-def'});
  fighterDef.innerText = 'Defense : ' + fighter.defense;
  const health = createElement({tagName:'span', className:'fighter-health'});
  health.innerText = 'Health : ' + fighter.health;
  const image = createElement({tagName:'img',className:'fighter-image', attributes : {src : fighter.source, alt : 'fighter'}});
  // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  fighterDetails.append(image,nameElement, fighterAttack, fighterDef, health);

  return fighterDetails;
}