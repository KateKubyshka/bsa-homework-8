export interface IShowModel{
    title : string;
    bodyElement : HTMLElement;
}