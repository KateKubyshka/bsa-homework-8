export interface ICreateElement{
    tagName : string;
    className? : string;
    attributes?: IDictionary;
}
interface IDictionary{
    [key:string]:string;
}